//
//  ViewController.swift
//  Brent
//
//  Created by Jamie Pinkham on 4/5/16.
//  Copyright © 2016 Jamie Pinkham. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UITableViewController {

    var results = [String]()
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var searchField: UISearchBar?
    @IBOutlet weak var refreshButton: UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let searchField = searchField, refreshButton = refreshButton {
        
            let search: Observable<String> = searchField.rx_text
                .throttle(0.3, scheduler: MainScheduler.instance)
                .distinctUntilChanged()
                .filter { text in return text.characters.count > 3 }
            
            refreshButton.rx_tap.subscribeNext { [weak self] in
                self?.results = [String]()
                self?.tableView.reloadData()
            }.addDisposableTo(disposeBag)
            
            let refresh = refreshButton.rx_tap.withLatestFrom(search)
            
            let merged = Observable.of(search, refresh).merge()
            
            merged.flatMapLatest { string in
                return Fetcher.fetchItems(string)
            }.observeOn(MainScheduler.instance)
            .map { results in
                return results.map { result in return result.title }
            }.subscribeNext { [weak self] titles in
                self?.results = titles
                self?.tableView.reloadData()
            }.addDisposableTo(disposeBag)
            
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.results.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        cell.textLabel?.text = self.results[indexPath.row]
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

