//
//  Fetcher.swift
//  Brent
//
//  Created by Jamie Pinkham on 4/5/16.
//  Copyright © 2016 Jamie Pinkham. All rights reserved.
//

import Foundation
import RxSwift

struct Result {
    let text: String
    let url: NSURL
}

struct Fetcher {
    static func fetchItems(query: String) -> Observable<[WikipediaSearchResult]> {
        return Observable.create { observer in
            let escapedQuery = query.URLEscaped
            let urlContent = "https://en.wikipedia.org/w/api.php?action=opensearch&search=\(escapedQuery)"
            let url = NSURL(string: urlContent)!
            let request = NSURLSession.sharedSession().dataTaskWithURL(url) { data, response, error in
                guard error == nil,
                    let data = data,
                    let json = try? NSJSONSerialization.JSONObjectWithData(data, options: []),
                    let array =  json as? [AnyObject],
                    let items = try? WikipediaSearchResult.parseJSON(array)
                else {
                    observer.onError(WikipediaParseError)
                    return
                }
                sleep(1)
                observer.onNext(items)
                observer.onCompleted()
            }
            
            
            request.resume()
            
            return AnonymousDisposable {
                request.cancel()
            }
        }
    }
}

extension String {
    var URLEscaped: String {
        return self.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet()) ?? ""
    }
}

func apiError(error: String) -> NSError {
    return NSError(domain: "WikipediaAPI", code: -1, userInfo: [NSLocalizedDescriptionKey: error])
}

public let WikipediaParseError = apiError("Error during parsing")


struct WikipediaSearchResult: CustomDebugStringConvertible {
    let title: String
    let description: String
    let URL: NSURL
    
    init(title: String, description: String, URL: NSURL) {
        self.title = title
        self.description = description
        self.URL = URL
    }
    
    // tedious parsing part
    static func parseJSON(json: [AnyObject]) throws -> [WikipediaSearchResult] {
        let rootArrayTyped = json.map { $0 as? [AnyObject] }
            .filter { $0 != nil }
            .map { $0! }
        
        if rootArrayTyped.count != 3 {
            throw WikipediaParseError
        }
        
        let titleAndDescription = Array(zip(rootArrayTyped[0], rootArrayTyped[1]))
        let titleDescriptionAndUrl: [((AnyObject, AnyObject), AnyObject)] = Array(zip(titleAndDescription, rootArrayTyped[2]))
        
        let searchResults: [WikipediaSearchResult] = try titleDescriptionAndUrl.map ( { result -> WikipediaSearchResult in
            let (first, url) = result
            let (title, description) = first
            
            guard let titleString = title as? String,
                let descriptionString = description as? String,
                let urlString = url as? String,
                let URL = NSURL(string: urlString) else {
                    throw WikipediaParseError
            }
            
            return WikipediaSearchResult(title: titleString, description: descriptionString, URL: URL)
        })
        
        return searchResults
    }
}

extension WikipediaSearchResult {
    var debugDescription: String {
        return "[\(title)](\(URL))"
    }
}

